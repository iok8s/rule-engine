$('#set_rule').submit(function (e) {
    e.preventDefault();
    var theForm = { };
    $.each($('#set_rule').serializeArray(), function() {
        theForm[this.name] = this.value;
    });
    let device_id = theForm["device_id"]
    if (device_id == "") {
        alert("El identificador no puede estar vacío")
        return false
    }
    delete theForm['device_id'];
    theForm["blocked"] = (theForm["blocked"] == "true")
    theForm["fields"] = theForm["fields"].split(",")
    theForm["maxSize"] = parseInt(theForm["maxSize"])

    $.ajax({
        url: "/rule/" + device_id,
        data: JSON.stringify(theForm),
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data, textStatus) {
            alert("Regla guardada")
            location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
            alert("Error guardando la regla:")
        }
    });
    return false
});