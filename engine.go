package main

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
)

type Rule struct {
	Blocked bool     `json:"blocked"`
	Fields  []string `json:"fields"`
	MaxSize int      `json:"maxSize"`
}

type RuleEngine struct {
	dbClient *redis.Client
}

func (e RuleEngine) fieldExist(id string, field string) (bool, error) {
	exists, err := e.dbClient.HExists(context.Background(), fmt.Sprintf("rule:%s", id), field).Result()
	switch {
	case err == redis.Nil:
		// id does not exist
		return false, fmt.Errorf("id %q doesn't exist", id)
	case err != nil:
		return false, fmt.Errorf("error getting the field %q value from id %q, %v", field, id, err)
	case !exists:
		return false, nil
	}
	return true, nil
}

// If the id doesn't have a "blocked" field or doesn't exist, it is not blocked by default
func (e RuleEngine) idIsBlocked(id string) (bool, error) {
	val, err := getFieldValue(e, id, "blocked")
	if err != nil {
		return false, err
	}
	if val == "" {
		return false, nil
	}

	blocked, err := strconv.ParseBool(val)
	switch {
	case err != nil:
		// Block the device as it has a blocked field which is not "false" or 0
		return true, fmt.Errorf("error parsing the blocked field value %q, %q", val, err)
	case blocked:
		return true, nil
	}
	return false, nil
}

func (e RuleEngine) getMaxSize(id string) (int, error) {
	val, err := getFieldValue(e, id, "maxSize")
	if err != nil {
		return 0, err
	}
	if val == "" {
		return 0, err
	}
	maxSize, err := strconv.Atoi(val)
	if err != nil {
		return 0, fmt.Errorf("error parsing the maxSize field value %q, %q", val, err)
	}
	return maxSize, nil
}

// Returns true if all the fields in the database match the JSON fields
func (e RuleEngine) blockedByFields(id string, fields []string) (bool, error) {
	val, err := getFieldValue(e, id, "fields")
	if err != nil {
		return false, err
	}
	if val == "" {
		return false, nil
	}
	msgFields := strings.Split(val, ",")
	if len(msgFields) < 1 {
		return false, fmt.Errorf("invalid fields value %q", val)
	}

	for _, field := range fields {
		if !contains(msgFields, field) {
			// If the field value in the database isn't inside the fields slice, block the message
			return true, nil
		}
	}
	return false, nil
}

func (e RuleEngine) getRule(id string) (Rule, error) {
	var rule Rule
	val, err := e.dbClient.HGetAll(context.Background(), fmt.Sprintf("rule:%s", id)).Result()
	if err == nil && len(val) == 0 {
		log.Debugf("id %q doesn't exist", id)
		return Rule{}, nil
	}
	if err != nil {
		return Rule{}, fmt.Errorf("error looking for the rule for id %q, %q", id, err)
	}
	rule.Blocked, err = strconv.ParseBool(val["blocked"])
	if err != nil {
		return Rule{}, fmt.Errorf("error parsing the blocked field %q for the rule for id %q, %q", val["blocked"], id, err)
	}
	rule.Fields = strings.Split(val["fields"], ",")
	rule.MaxSize, err = strconv.Atoi(val["maxSize"])
	if err != nil {
		return Rule{}, fmt.Errorf("error parsing the maxSize field %q for the rule for id %q, %q", val["maxSize"], id, err)
	}
	return rule, nil
}

func (e RuleEngine) setRule(id string, rule Rule) error {
	_, err := e.dbClient.HSet(context.Background(), fmt.Sprintf("rule:%s", id), "blocked", strconv.FormatBool(rule.Blocked), "fields", strings.Join(rule.Fields, ","), "maxSize", strconv.Itoa(rule.MaxSize)).Result()
	return err
}

func (e RuleEngine) listRules() (map[string]Rule, error) {
	var cursor uint64
	var keys []string
	ctx := context.Background()
	for {
		var err error
		var subKeys []string
		subKeys, cursor, err = e.dbClient.Scan(ctx, cursor, "rule:*", 10).Result()
		if err != nil {
			panic(err)
		}
		keys = append(keys, subKeys...)
		if cursor == 0 {
			break
		}
	}
	rules := make(map[string]Rule)
	for _, key := range keys {
		rule, err := e.getRule(strings.TrimPrefix(key, "rule:"))
		if err != nil {
			return map[string]Rule{}, err
		}
		rules[strings.TrimPrefix(key, "rule:")] = rule
	}
	return rules, nil
}

func getFieldValue(e RuleEngine, id string, field string) (string, error) {
	fieldExists, err := e.fieldExist(id, field)
	if err != nil {
		return "", fmt.Errorf("error looking for the %q field, %q", field, err)
	}
	if !fieldExists {
		return "", nil
	}

	val, err := e.dbClient.HGet(context.Background(), fmt.Sprintf("rule:%s", id), field).Result()
	switch {
	case err == redis.Nil:
		// id does not exist
		return "", fmt.Errorf("id %q doesn't exist", id)
	case err != nil:
		return "", fmt.Errorf("error getting id, %q", err)
	}

	return val, nil
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
