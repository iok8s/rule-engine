package main

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"testing"

	"github.com/alicebob/miniredis/v2"
	"github.com/go-redis/redis/v8"
)

const id = "random_id"

var redisServer *miniredis.Miniredis

var (
	nonblockedRule = Rule{
		Blocked: false,
		Fields:  []string{""},
		MaxSize: 0,
	}
	blockedRule = Rule{
		Blocked: true,
		Fields:  []string{""},
		MaxSize: 0,
	}
	validSizeRule = Rule{
		Blocked: false,
		Fields:  []string{""},
		MaxSize: 200,
	}
	validFieldsRule = Rule{
		Blocked: false,
		Fields:  []string{"wanted1", "wanted2"},
		MaxSize: 200,
	}
	emptyFieldsRule = Rule{
		Blocked: false,
		Fields:  []string{""},
		MaxSize: 200,
	}
)

func checkResult(t *testing.T, err error, expectedAnError bool, got interface{}, want interface{}) {
	if err != nil && !expectedAnError {
		t.Errorf("didn't expect an error %q", err)
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
	}
}

func initRedisMock(devices map[string]Rule) *redis.Client {
	s, err := miniredis.Run()
	redisServer = s
	if err != nil {
		panic(err)
	}

	// Initialize the database
	for id, rule := range devices {
		redisServer.HSet(fmt.Sprintf("rule:%s", id), "blocked", strconv.FormatBool(rule.Blocked), "fields", strings.Join(rule.Fields, ","), "maxSize", strconv.Itoa(rule.MaxSize))
	}

	rdb := redis.NewClient(&redis.Options{
		Addr: redisServer.Addr(),
	})
	return rdb
}

func tearDownRedisMock() {
	redisServer.Close()
}
