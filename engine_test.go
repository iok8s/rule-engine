package main

import (
	"context"
	"fmt"
	"testing"
)

func TestFieldExist(t *testing.T) {
	type test struct {
		name     string
		database map[string]Rule
		field    string
		want     bool
	}
	testCases := []test{
		{
			name:     "id doesn't exist",
			database: nil,
			field:    "blocked",
			want:     false,
		},
		{
			name:     "id exists and field exists",
			database: map[string]Rule{id: nonblockedRule},
			field:    "blocked",
			want:     true,
		},
		{
			name:     "id exists and field doesn't exist",
			database: map[string]Rule{id: nonblockedRule},
			field:    "dummyblocked",
			want:     false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			rdb := initRedisMock(testCase.database)
			defer tearDownRedisMock()
			s := RuleEngine{rdb}

			found, err := s.fieldExist(id, testCase.field)
			checkResult(t, err, false, found, testCase.want)
		})
	}
}

func TestGetRule(t *testing.T) {
	type test struct {
		name     string
		database map[string]Rule
		want     Rule
	}
	testCases := []test{
		{
			name:     "id doesn't exist",
			database: nil,
			want:     Rule{},
		},
		{
			name:     "id exists",
			database: map[string]Rule{id: validFieldsRule},
			want:     validFieldsRule,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			rdb := initRedisMock(testCase.database)
			defer tearDownRedisMock()
			s := RuleEngine{rdb}

			rule, err := s.getRule(id)
			checkResult(t, err, false, rule, testCase.want)
		})
	}
}

func TestIdIsBlocked(t *testing.T) {
	type test struct {
		name     string
		database map[string]Rule
		id       string
		want     bool
	}
	testCases := []test{
		{
			name:     "id doesn't exist",
			database: map[string]Rule{id: nonblockedRule},
			id:       "dummyid",
			want:     false,
		},
		{
			name:     "id exists and isn't blocked",
			database: map[string]Rule{id: nonblockedRule},
			id:       id,
			want:     false,
		},
		{
			name:     "id exists and is blocked",
			database: map[string]Rule{id: blockedRule},
			id:       id,
			want:     true,
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			rdb := initRedisMock(testCase.database)
			defer tearDownRedisMock()
			s := RuleEngine{rdb}

			blocked, err := s.idIsBlocked(testCase.id)
			checkResult(t, err, false, blocked, testCase.want)
		})
	}
	t.Run("if blocked field doesn't exist the id is not blocked", func(t *testing.T) {
		rdb := initRedisMock(map[string]Rule{"": nonblockedRule})
		defer tearDownRedisMock()
		rdb.HSet(context.Background(), fmt.Sprintf("rule:%s", id), "dummyblocked", "true")

		s := RuleEngine{rdb}

		blocked, err := s.idIsBlocked(id)
		checkResult(t, err, false, blocked, false)
	})
}

func TestGetMaxSize(t *testing.T) {
	type test struct {
		name          string
		database      map[string]Rule
		id            string
		size          int
		expectedError bool
		want          int
	}
	testCases := []test{
		{
			name:          "id doesn't exist",
			database:      map[string]Rule{id: validSizeRule},
			id:            "dummyid",
			size:          0,
			expectedError: false,
			want:          0,
		},
		{
			name:          "id exists and size is lesser than maxSize",
			database:      map[string]Rule{id: validSizeRule},
			id:            id,
			expectedError: false,
			want:          200,
		},
		{
			name:          "id exists and size is higher than maxSize",
			database:      map[string]Rule{id: validSizeRule},
			id:            id,
			expectedError: false,
			want:          200,
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			rdb := initRedisMock(testCase.database)
			defer tearDownRedisMock()
			s := RuleEngine{rdb}

			maxSize, err := s.getMaxSize(testCase.id)
			checkResult(t, err, testCase.expectedError, maxSize, testCase.want)
		})
	}
}

func TestBlockedByFields(t *testing.T) {
	type test struct {
		name          string
		database      map[string]Rule
		id            string
		fields        []string
		expectedError bool
		want          bool
	}
	testCases := []test{
		{
			name:          "id doesn't exist",
			database:      map[string]Rule{id: validFieldsRule},
			id:            "dummyid",
			fields:        []string{""},
			expectedError: false,
			want:          false,
		},
		{
			name:          "id exists fields value is empty",
			database:      map[string]Rule{id: emptyFieldsRule},
			id:            id,
			fields:        []string{""},
			expectedError: false,
			want:          false,
		},
		{
			name:          "id exists and fields don't match",
			database:      map[string]Rule{id: validFieldsRule},
			id:            id,
			fields:        []string{"unwanted1", "unwanted2"},
			expectedError: false,
			want:          true,
		},
		{
			name:          "id exists and fields match",
			database:      map[string]Rule{id: validFieldsRule},
			id:            id,
			fields:        []string{"wanted1", "wanted2"},
			expectedError: false,
			want:          false,
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			rdb := initRedisMock(testCase.database)
			defer tearDownRedisMock()
			s := RuleEngine{rdb}

			blocked, err := s.blockedByFields(testCase.id, testCase.fields)
			checkResult(t, err, testCase.expectedError, blocked, testCase.want)
		})
	}
}

func TestSetRule(t *testing.T) {
	type test struct {
		name          string
		database      map[string]Rule
		id            string
		rule          Rule
		expectedError bool
		want          map[string]Rule
	}
	testCases := []test{
		{
			name:          "id doesn't exist",
			database:      map[string]Rule{},
			id:            id,
			rule:          validFieldsRule,
			expectedError: false,
			want:          map[string]Rule{id: validFieldsRule},
		},
		{
			name:          "id exists",
			database:      map[string]Rule{id: blockedRule},
			id:            id,
			rule:          validFieldsRule,
			expectedError: false,
			want:          map[string]Rule{id: validFieldsRule},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			rdb := initRedisMock(testCase.database)
			defer tearDownRedisMock()
			s := RuleEngine{rdb}

			err := s.setRule(testCase.id, testCase.rule)
			checkResult(t, err, testCase.expectedError, nil, nil)
		})
	}
}

func TestListRules(t *testing.T) {
	type test struct {
		name     string
		database map[string]Rule
	}
	testCases := []test{
		{
			name:     "no ids",
			database: map[string]Rule{},
		},
		{
			name:     "id exists",
			database: map[string]Rule{id: validFieldsRule, "dummyid": validSizeRule},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			rdb := initRedisMock(testCase.database)
			defer tearDownRedisMock()
			s := RuleEngine{rdb}

			rules, err := s.listRules()
			checkResult(t, err, false, rules, testCase.database)
		})
	}
}
