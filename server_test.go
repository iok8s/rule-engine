package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"
)

func TestPostBlocked(t *testing.T) {
	t.Run("post method is not allowed", func(t *testing.T) {
		rdb := initRedisMock(nil)
		e := RuleEngine{rdb}
		server := &RuleServer{&e}

		request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("/auth/%s", id), bytes.NewBuffer([]byte(`{"a": "b"}`)))
		request.Header.Set("Content-Type", "application/json")
		response := httptest.NewRecorder()

		server.ServeHTTP(response, request)

		assertStatus(t, response.Code, http.StatusMethodNotAllowed)

	})
}

func TestGetBlockedId(t *testing.T) {
	var tests = []struct {
		name           string
		jsonStr        []byte
		expectedStatus int
		database       map[string]Rule
	}{
		{
			name:           "returns 403 if id is blocked",
			jsonStr:        []byte(`{"a":"aa", "b": "bb"}`),
			database:       map[string]Rule{id: blockedRule},
			expectedStatus: http.StatusForbidden,
		},
		{
			name:           "returns 403 if size is too big",
			jsonStr:        make([]byte, 1000),
			database:       map[string]Rule{id: validSizeRule},
			expectedStatus: http.StatusForbidden,
		},
		{
			name:           "returns 403 if not enough fields",
			jsonStr:        []byte(`{"unwanted1": "a", "wanted2": "b"}`),
			database:       map[string]Rule{id: validFieldsRule},
			expectedStatus: http.StatusForbidden,
		},
		{
			name:           "returns 400 if cannot read the body",
			jsonStr:        []byte(`{""}`),
			database:       map[string]Rule{id: validFieldsRule},
			expectedStatus: http.StatusBadRequest,
		},
		{
			name:           "returns 200 if id does not exist",
			jsonStr:        []byte(`{"wanted1": "a", "wanted2": "b"}`),
			database:       map[string]Rule{"dummyid": validFieldsRule},
			expectedStatus: http.StatusOK,
		},
		{
			name:           "returns 200 if id exists, is not blocked and message is valid",
			jsonStr:        []byte(`{"wanted1": "a", "wanted2": "b"}`),
			database:       map[string]Rule{"id": validFieldsRule},
			expectedStatus: http.StatusOK,
		},
	}
	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			rdb := initRedisMock(testCase.database)
			e := RuleEngine{rdb}
			server := &RuleServer{&e}

			request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/auth/%s", id), bytes.NewBuffer(testCase.jsonStr))
			request.Header.Set("Content-Type", "application/json")
			response := httptest.NewRecorder()

			server.ServeHTTP(response, request)

			assertStatus(t, response.Code, testCase.expectedStatus)

		})
	}
}

func TestGetRuleId(t *testing.T) {
	var tests = []struct {
		name           string
		expectedStatus int
		database       map[string]Rule
		want           string
	}{
		{
			name:           "returns 200 and rule if exists",
			database:       map[string]Rule{id: validFieldsRule},
			expectedStatus: http.StatusOK,
			want:           `{"blocked":false,"fields":["wanted1","wanted2"],"maxSize":200}`,
		},
		{
			name:           "returns 400 if user don't exist",
			database:       map[string]Rule{"dummyid": validFieldsRule},
			expectedStatus: http.StatusNotFound,
			want:           http.StatusText(http.StatusNotFound),
		},
	}
	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			rdb := initRedisMock(testCase.database)
			e := RuleEngine{rdb}
			server := &RuleServer{&e}

			request, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/rule/%s", id), nil)
			request.Header.Set("Content-Type", "application/json")
			response := httptest.NewRecorder()

			server.ServeHTTP(response, request)

			assertStatus(t, response.Code, testCase.expectedStatus)
			if strings.TrimSuffix(response.Body.String(), "\n") != testCase.want {
				t.Errorf("got %q want %q", response.Body, testCase.want)
			}

		})
	}
}

func TestGetRules(t *testing.T) {
	var tests = []struct {
		name           string
		expectedStatus int
		database       map[string]Rule
	}{
		{
			name:           "returns 200 and rules if exists",
			database:       map[string]Rule{id: validFieldsRule, "dummyid": validSizeRule},
			expectedStatus: http.StatusOK,
		},
		{
			name:           "returns 200 and empty response if not exists",
			database:       map[string]Rule{},
			expectedStatus: http.StatusOK,
		},
	}
	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			rdb := initRedisMock(testCase.database)
			e := RuleEngine{rdb}
			server := &RuleServer{&e}

			request, _ := http.NewRequest(http.MethodGet, "/rules", nil)
			request.Header.Set("Content-Type", "application/json")
			response := httptest.NewRecorder()

			server.ServeHTTP(response, request)

			assertStatus(t, response.Code, testCase.expectedStatus)

			var result map[string]Rule
			err := json.NewDecoder(response.Body).Decode(&result)
			if err != nil {
				t.Error("error decoding json ", err)
			}
			if !reflect.DeepEqual(result, testCase.database) {
				t.Errorf("got %v want %v", result, testCase.database)
			}

		})
	}
}

func TestPostRuleId(t *testing.T) {
	var tests = []struct {
		name           string
		expectedStatus int
		database       map[string]Rule
		jsonStr        []byte
		want           Rule
	}{
		{
			name:           "returns 200 if id doesn't exist and the rule is saved",
			database:       map[string]Rule{},
			jsonStr:        []byte(`{"blocked":false,"fields":["wanted1","wanted2"],"maxSize":200}`),
			expectedStatus: http.StatusOK,
			want:           validFieldsRule,
		},
		{
			name:           "returns 200 if id exists and the rule is updated",
			database:       map[string]Rule{id: emptyFieldsRule},
			jsonStr:        []byte(`{"blocked":false,"fields":["wanted1","wanted2"],"maxSize":200}`),
			expectedStatus: http.StatusOK,
			want:           validFieldsRule,
		},
		{
			name:           "returns 422 if rule is not valid",
			database:       map[string]Rule{id: validFieldsRule},
			jsonStr:        []byte(`{"blocked": 200}`),
			expectedStatus: http.StatusUnprocessableEntity,
			want:           validFieldsRule,
		},
	}
	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			rdb := initRedisMock(testCase.database)
			e := RuleEngine{rdb}
			server := &RuleServer{&e}

			request, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("/rule/%s", id), bytes.NewBuffer(testCase.jsonStr))
			request.Header.Set("Content-Type", "application/json")
			response := httptest.NewRecorder()

			server.ServeHTTP(response, request)

			assertStatus(t, response.Code, testCase.expectedStatus)
			savedRule, err := e.getRule(id)
			if err != nil {
				t.Errorf("got error %v while getting the rule", err)
			}
			if !reflect.DeepEqual(savedRule, testCase.want) {
				t.Errorf("got %v want %v", savedRule, testCase.want)
			}

		})
	}
}

func assertStatus(t *testing.T, got, want int) {
	if got != want {
		t.Errorf("got status %v, want %v", got, want)
	}
}
