package main

import (
	"encoding/json"
	"errors"
	"html/template"
	"io/ioutil"
	"net/http"
	"reflect"

	log "github.com/sirupsen/logrus"
)

func init() {
	log.SetLevel(log.DebugLevel)
}

type RuleServer struct {
	ruleEngine *RuleEngine
}

type Results struct {
	Total int             `json:"total"`
	Rules map[string]Rule `json:"rules"`
}

var tpl = template.Must(template.ParseFiles("assets/index.html"))

func (c *RuleServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	router := http.NewServeMux()
	router.Handle("/auth/", http.HandlerFunc(c.isAble))
	router.Handle("/rule/", http.HandlerFunc(c.manageRules))
	router.Handle("/rules", http.HandlerFunc(c.getRules))
	router.Handle("/", http.HandlerFunc(c.indexHandler))

	fs := http.FileServer(http.Dir("./assets"))
	router.Handle("/assets/", http.StripPrefix("/assets", fs))

	router.ServeHTTP(w, r)
}

func (c *RuleServer) indexHandler(w http.ResponseWriter, r *http.Request) {
	log.Debug("webapp rules", r.Method)
	tpl = template.Must(template.ParseFiles("assets/index.html"))

	rules, err := c.ruleEngine.listRules()
	if err != nil {
		log.Debugf("error getting rules: %v", err)
		tpl.Execute(w, nil)
		return
	}
	results := Results{
		Total: len(rules),
		Rules: rules,
	}
	tpl.Execute(w, results)
}

func (c *RuleServer) isAble(w http.ResponseWriter, r *http.Request) {
	log.Debug("isAble", r.Method)
	if r.Method != http.MethodGet {
		log.Debugf("method %q not allowed", r.Method)
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}
	// get id and check if it's blocked
	id := r.URL.Path[len("/auth/"):]
	blocked, err := c.ruleEngine.idIsBlocked(id)
	if err != nil {
		log.Debugf("error checking if id %q is blocked: %v", id, err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if blocked {
		log.Debugf("id %q is blocked", id)
		w.WriteHeader(http.StatusForbidden)
		return
	}

	// check if request body is not too large
	maxBytesSize, err := c.ruleEngine.getMaxSize(id)
	if err != nil {
		log.Debugf("error getting id %q maxSize: %v", id, err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	if maxBytesSize != 0 {
		r.Body = http.MaxBytesReader(w, r.Body, int64(maxBytesSize))
	}
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		if err.Error() == "http: request body too large" {
			w.WriteHeader(http.StatusForbidden)
			log.Debug("request too big")
			return
		}
		log.Debug("error reading body, ", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	if string(data) == "" {
		log.Debug("empty body")
		http.Error(w, "empty body", http.StatusBadRequest)
		return
	}

	// check if it contains the necessary fields
	var dataAsJson map[string]json.RawMessage
	err = json.Unmarshal(data, &dataAsJson)
	if err != nil {
		log.Debug("cannot decode body ", err)
		http.Error(w, "cannot decode body", http.StatusBadRequest)
		return
	}

	var msgFields []string
	for field := range dataAsJson {
		msgFields = append(msgFields, field)
	}

	blocked, err = c.ruleEngine.blockedByFields(id, msgFields)
	if err != nil {
		log.Debugf("error checking if id %q fields %q areenough: %v", id, msgFields, err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if blocked {
		log.Debugf("id %q fields %q are blocked", id, msgFields)
		w.WriteHeader(http.StatusForbidden)
		return
	}

	// if it has passed every check return an OK status
	w.WriteHeader(http.StatusOK)
}

func (c *RuleServer) manageRules(w http.ResponseWriter, r *http.Request) {
	log.Debug("manageRules", r.Method)
	if r.Method == http.MethodGet {
		c.getRule(w, r)
		return
	}
	if r.Method == http.MethodPost {
		c.setRule(w, r)
		return
	}
	log.Debugf("method %q not allowed", r.Method)
	http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
}

func (c *RuleServer) getRule(w http.ResponseWriter, r *http.Request) {
	log.Debug("getRule", r.Method)
	id := r.URL.Path[len("/rule/"):]
	rule, err := c.ruleEngine.getRule(id)
	if err != nil {
		log.Debugf("error getting id %q rule: %v", id, err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	if reflect.DeepEqual(rule, Rule{}) {
		log.Debugf("id %q not found", id)
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}

	json.NewEncoder(w).Encode(rule)
	w.Header().Set("Content-Type", "application/json")
}

func (c *RuleServer) getRules(w http.ResponseWriter, r *http.Request) {
	log.Debug("getRules", r.Method)
	rules, err := c.ruleEngine.listRules()
	if err != nil {
		log.Debugf("error getting rules: %v", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(rules)
	w.Header().Set("Content-Type", "application/json")
}

func (c *RuleServer) setRule(w http.ResponseWriter, r *http.Request) {
	log.Debug("setRule ", r.Method)
	log.Debug("setting rule")

	id := r.URL.Path[len("/rule/"):]

	body, err := parseBody(r)
	if err != nil {
		log.Debug(err)
		http.Error(w, err.Error(), http.StatusUnprocessableEntity)
		return
	}

	var rule Rule
	err = json.Unmarshal(body, &rule)
	log.Debug(string(body))

	if err != nil {
		log.Debug("setRule ", "cannot unmarshall body ", err)
		http.Error(w, http.StatusText(http.StatusUnprocessableEntity), http.StatusUnprocessableEntity)
		return
	}

	err = c.ruleEngine.setRule(id, rule)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	log.Debugf("rule %v set", rule)
	w.Write([]byte("{}"))
}

func parseBody(r *http.Request) ([]byte, error) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		return nil, err
	}
	if string(body) == "" {
		return nil, errors.New("empty body")
	}
	return body, nil
}
