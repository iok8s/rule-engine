# Rule Engine

This module acts as a REST API interface for authenticating in the system. You can create rules via REST API or web UI. These rules are based on three keys:

- `blocked`: if the ID is blocked or not.
- `fields`: if the message contains the necessary fields i.e "temperature" and "humidity".
- `maxSize`: the maximum size of the message in bytes.

## How to use it

Rules can be created via HTTP calls i.e

```
curl -d '{"blocked": false, "fields": ["temperature", "humidity"], "maxSize": 205}' -H "Content-Type: application/json" -X POST http://localhost:3000/rule/my-id
```

or using the web UI. There is an endpoint to list all the rules:

```
curl http://localhost:3000/rules
```

or a specific device id:

```
curl http://localhost:3000/rule/my-id
```

In order to check if an ID is blocked, the `auth` endpoint is used:

```
curl -d '{"temperature": 23.5, "humidity": 80}' -H "Content-Type: application/json" -X POST http://localhost:3000/auth/my-id
```

which then returns a 403 (forbidden) status code if the ID is blocked or the message doesn't comply with the set rule.