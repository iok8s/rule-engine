module engine_rule.com/m/v0

go 1.16

require (
	github.com/alicebob/miniredis v2.5.0+incompatible // indirect
	github.com/alicebob/miniredis/v2 v2.14.5 // indirect
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/go-redis/redis/v8 v8.9.0 // indirect
	github.com/gomodule/redigo v1.8.4 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
)
